package com.Nariah.eco;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.Nariah.Bunkers;

import net.md_5.bungee.api.ChatColor;

public class Balance implements CommandExecutor {

	Bunkers main;

	public Balance(Bunkers plugin) {
		main = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Players can only use this command.");
			return true;
		}

		if (args.length == 0) {
			Player p = (Player) sender;

			p.sendMessage(ChatColor.AQUA + "Balance: " + ChatColor.GREEN + EcoSetup.economy.getBalance(p.getName()));

		}
		
		
		return true;
	}

}
